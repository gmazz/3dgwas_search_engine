
import json
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Q

# This function builds the Elasticsearch DB from JSON.
def es_build(es):
    json_file = '../GWAS_db.json'
    with open(json_file) as json_data:
        data = json_data.read()

    # This istantiate the elasticsearch
    GWAS_list = json.loads(data)
    for nr, document in enumerate(GWAS_list):
        es.index(index='gw', doc_type='gwas', id=nr, body=document)

def es_query(es):

    # This is the query written using "elasticsearch"
    #res = es.search(index="gw", body={ "from" : 0, "size": 10000, "query": {"term": {"CHR_ID": 10}}})

    # This is the query written using "elasticseach-dsl"
    s = Search(using = es, index = "gw")
    s = s.filter('term', CHR_ID = 10)
    res_dsl = s.execute().to_dict()
    print res_dsl['hits']['total']

    #print es.get(index='sw', doc_type='gwas', id=1)
    #print len(res['hits']['hits'])

def es_query_illnesses(es, illnesses):
    s = Search(using = es, index = "gw")
    s.query = Q('bool', should=[Q('match', MAPPED_TRAIT=illness) for illness in illnesses])
    res_dsl = s.execute().to_dict()
    print res_dsl['hits']['total']

def main():
    es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
    # es_build(es)
    # es_query(es)
    es_query_illnesses(es, ['body mass index', 'lung carcinoma'])

if __name__ == '__main__':
    main()
