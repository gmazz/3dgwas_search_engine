
import json, requests
from elasticsearch import Elasticsearch

def es_request(data, query):
    # This istantiate the elasticsearch
    es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
    res = es.index(index='sw', doc_type='people', id=1, body=json.loads(str(data)))         
    print(res)

def main():
    #data = requests.get('http://swapi.co/api/people/'+ str(1))
    #print data.content

    json_file = 'test_data.json'
    with open(json_file) as json_data:
        test_data = json.load(json_data)
        query = ''
        #print test_data, type(test_data)
        #es_request(test_data, query)

if __name__ == '__main__':
    main()
