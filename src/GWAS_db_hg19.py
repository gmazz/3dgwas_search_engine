from utils import *

'''
Description:

    This script generates a mongoDB database containing the GWAS informations obtained from the
    GWAS cathalog, but converting these data into hg19 coordinates. Hence:

    1) Only entries with convertible SNPs (hg38 -> hg19) are included in the DB generates
    2) Domain relates data are also included 
'''

'''
######################### Function utilities for one-time use ########################:

# Functions used sporadically, for example functions used to generate an output data file that is further used without the need to execute the function multiple times.'''


'''
Description: the efo_gwas_dict_gen() function generates the EFO_GWAS_DICT pickle object which can be further used
'''
def efo_gwas_dict_gen():
    efo_gwas_dict = {}
    gwas_caa = csv2dict(GWAS_CAA_ON, '\t')
    gwas_efo = csv2dict(GWAS_EFO_MAP, '\t')
    for entry in gwas_caa:
         for efo in gwas_efo:
             if entry['DISEASE/TRAIT'] == efo['Disease trait']:
                try:
                    if efo not in efo_gwas_dict[entry['DISEASE/TRAIT']]:
                        efo_gwas_dict[entry['DISEASE/TRAIT']].append(efo)
                except:
                    try:
                        efo_gwas_dict[entry['DISEASE/TRAIT']] = [efo]
                    except:
                        print "something is wrong with %s" %entry['DISEASE/TRAIT']
                        break

    pickle_save(efo_gwas_dict, EFO_GWAS_DICT)
    

''' #############################  END  ############################## '''
''' This function get a nested dict (ndict) and generates a list of flattened dicts to be inserted within mongodb '''
def flatten_dict2(d, depth):
    if depth == 1:
        for i in d.values():
            yield i
    else:
        for v in d.values():
            if isinstance(v, dict):
                for i in flatten_dict2(v, depth-1):
                    yield i


''' ########### ESSENTIAL CODE: ############# '''

'''
Description: get_GWAS() returns the list of dict containing AI_related entries.
Such dict can be further used to insert the entries of interest into MongoDB
'''
def get_GWAS(trait_file):
    disease_traits = file2list(trait_file) # Path variable for the list of diseases
    gwas_caa = csv2dict(GWAS_CAA_ON, '\t')
    to_return =  [gwas for gwas in gwas_caa if gwas['DISEASE/TRAIT'] in disease_traits]
    print len(to_return)
    return to_return


'''
Description: efo_correct_missing assigns the EFO mapping to traits with missing efo mapping
'''
def efo_correct_missing(data_dict):
    efo_gwas_dict = pickle_load(EFO_GWAS_DICT)
    for gwas in data_dict:
        if gwas['MAPPED_TRAIT'] == '':
            try:
                gwas['MAPPED_TRAIT'] = efo_gwas_dict[gwas['DISEASE/TRAIT']][0]['EFO term']
                gwas['MAPPED_TRAIT_URI'] = efo_gwas_dict[gwas['DISEASE/TRAIT']][0]['EFO URI']  
            except:
                print "%s is not valid" %(efo_gwas_dict[gwas['DISEASE/TRAIT']][0]['EFO term'])
                break
    return data_dict


'''
Description: efo_trait_cat includes the EFO parent cathegory of each given trait, with associated URI.
'''
def efo_trait_cat(data_dict):
    efo_gwas_dict = pickle_load(EFO_GWAS_DICT)
    for gwas in data_dict:
        try:
            efo_cat = list(set([j['Parent term'] for j in efo_gwas_dict[gwas['DISEASE/TRAIT']]]))
            efo_cat_uri = list(set([j['Parent URI'] for j in efo_gwas_dict[gwas['DISEASE/TRAIT']]]))
            gwas['CATEGORIES'] = efo_cat
            gwas['CATEGORIES_URI'] = efo_cat_uri
        except:
            print "%s is not valid" %(efo_gwas_dict[gwas['DISEASE/TRAIT']][0]['EFO term'])
            break
    return data_dict


''' The following function get both: the list of already pre-processed entries from GWAS (for mongoDB insertion) and the file for SNP conversion
He does prepare a new mongoDB with GWAS converted to hg19 and associated domains.
'''
def SNP_coords_tranform(data_dict, fields_selection):
    new_data_dict = [] 
    SNP_map = pickle_load(SNP_mapping_file)
    for d in data_dict:
       try:
           rs = d['SNP_ID_CURRENT']
           if rs:
               query = ''.join(('rs', rs))
               if SNP_map[query]:
                   SNP_new_chr, SNP_new_pos = SNP_map[query].split("'")[1].split("-")[0].split(":")
                   new_d = {k: d[k] for k in fields_selection}
                   new_d['CHR_ID'] = SNP_new_chr
                   new_d['CHR_POS'] = SNP_new_pos
                   new_data_dict.append(new_d)
               else:
                   print query, 'No data are available for this query'
       except:
           pass
    
    return new_data_dict

'''
Description: This function takes in input the data and add a numerical ID  
'''
def ID_enum(data_dict):
    for i, elem in enumerate(data_dict):
        elem['GWAS_ID'] = i
    return data_dict

'''
Description: this functions is a parser taking a gene-name string and returns a list of genes
'''

def gene_str_list(data_dict):
    for elem in data_dict:
        elem['MAPPED_GENE'] = [g for g in re.split('[ - |,]', elem['MAPPED_GENE'], flags=re.IGNORECASE) if g not in ['-', ',', '']] 
    return data_dict


''' Description : ... '''

def get_domains_dict(domain_dict):
    data_dict = {}
    for k, v in domain_dict.iteritems():
        try:
            data_dict[v['chr']].update({k: v})
        except:
            try:
                data_dict[v['chr']] = {k: v}
            except:
                print "Something is wrong with %s" %k
                break
    return data_dict


'''
Description: Includes TAD definitions on each SNP
'''
def SNP_TAD_mapping(data_dict, k):
    files = {'CCD': CCD, 'RAO': RAO, 'IMR90': IMR90}
    # A dict containing the dicts of the domain data present in the "files" dict.
    TAD_data = {fn: get_domains_dict(csv2dict_simple(f, '\t')) for fn, f in files.iteritems()} 
    GWAS_legal = [entry for entry in data_dict if entry['CHR_POS']]
    for entry in GWAS_legal:
        entry['DOMAINS'] = []
        chr_ID = ''.join(['chr', entry['CHR_ID']])
        try:
            for tad_id, tad in TAD_data[k][chr_ID].iteritems():
                if int(entry['CHR_POS']) >= int(tad['start']) and int(entry['CHR_POS']) <= int(tad['end']):
                    entry['DOMAINS'].append(tad)
                else:
                    pass
        except:
            pass
    return data_dict


'''
Description: write data into mongoDB
'''
def write_mongoDB(new_data_dict):
    DB_name = DB_NAME
    collection_name = COLLECTION_NAME
    try:
        mongo_drop_collection(DB_name, collection_name)
        mongo_insert(DB_name, collection_name, new_data_dict)
        print '\nThe mongoDB containing the database %s and collection %s was generated.\n' %(DB_name, collection_name)
    except:
        print '\nOps, a problem occoured while building your marvelous mongoDB! Please check what went wrong\n'


def json_export(filename, data_list):
    data_to_export = {'_'.join(('id', str(k))):v for k,v in enumerate(data_list)}
    json_dump(GWAS_DB_JSON, data_to_export)


def json_export_2(filename, data_list):
    for k, d in enumerate(data_list):
        d['id'] = str(k)
    json_dump(GWAS_DB_JSON, data_list)


def main():
    trait_file = ALL_DISEASES
    data_dict = get_GWAS(trait_file)

    ''' This contains the selection of fields to be included in the hg19 database 
    (e.g. downstream, upstream gene distances are excluded because are unknown after the SNP coordinate conversion)'''
    
    fields_selection = ['CNV',
    'OR or BETA',
    'SNP_GENE_IDS',
    'DISEASE/TRAIT',
    'MAPPED_TRAIT_URI',
    'CATEGORIES',
    'CATEGORIES_URI',
    'PLATFORM [SNPS PASSING QC]',
    'LINK',
    'CONTEXT',
    'DATE',
    'P-VALUE (TEXT)',
    '95% CI (TEXT)', 
    'FIRST AUTHOR',
    'CHR_ID',
    'INTERGENIC',
    'PUBMEDID',
    'SNP_ID_CURRENT',
    'INITIAL SAMPLE SIZE',
    'MERGED',
    'PVALUE_MLOG',
    'SNPS',
    'MAPPED_TRAIT',
    'STUDY',
    'RISK ALLELE FREQUENCY',
    'CHR_POS',
    'MAPPED_GENE',
    'P-VALUE',
    'STRONGEST SNP-RISK ALLELE',
    'REPORTED GENE(S)',
    'JOURNAL',
    'REPLICATION SAMPLE SIZE',
    'DATE ADDED TO CATALOG',
    'REGION']
   
    data_dict = efo_correct_missing(data_dict)
    data_dict = efo_trait_cat(data_dict)     

    new_data_dict = SNP_coords_tranform(data_dict, fields_selection)
    new_data_dict = SNP_TAD_mapping(new_data_dict, 'CCD')
    new_data_dict = ID_enum(new_data_dict)
    new_data_dict = gene_str_list(new_data_dict)

    ''' Section for final data generation (MONGODB or JSON)'''
    
    write_mongoDB(new_data_dict)

    #json_export_2(GWAS_DB_JSON, new_data_dict) # This exports list of dict with id
    #json_export(GWAS_DB_JSON, new_data_dict) # This exports dict of dicts with id

if __name__ == '__main__':
    
    ''' Functions for the generation of serialized objects (.pkl). These functions are run sporadically.'''
    #efo_gwas_dict_gen()
    
    main()
