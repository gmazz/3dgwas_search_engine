'''
    This script is dedicated to the extraction of information from mongoDB
'''

from utils import *


def get_domains_only(DB_name, collection_name):
    client = MongoClient()
    db = client[DB_name]
    collection = db[collection_name]
    return list(collection.find({"DOMAINS": {"$exists": "true", "$not": {"$size": 0}}}))

def flat_nested_list(mylist):
    return list(set([x for y in mylist for x in y]))


def mongo_aggregate(data_list):
    d = {}
    for elem in data_list:
        try:
            d[elem['SNP_ID_CURRENT']].append(elem)
        except:
            d[elem['SNP_ID_CURRENT']] = [elem]
    
    for i, v in enumerate(d):
        tmp_CHR_IDs = list(set([j['CHR_ID'] for j in d[v]]))[0]
        tmp_traits = flast_nested_list([j['']])
        
        print i,v,tmp_CHR_IDs     


def main():
    GWAS_only_domains = get_domains_only(DB_NAME, COLLECTION_NAME)
    mongo_aggregate(GWAS_only_domains)
    print 'THIS piece of code is not completed yet. The mongo_aggregate function need additional work.'


if __name__ == '__main__':
    main()
