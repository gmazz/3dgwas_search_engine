from utils import *

# This function builds the Elasticsearch DB from JSON.
def es_build(es):
    json_file = '../GWAS_db.json'
    with open(json_file) as json_data:
        data = json_data.read()

    # This istantiate the elasticsearch
    GWAS_list = json.loads(data)
    for nr, document in enumerate(GWAS_list):
        es.index(index='gw', doc_type='gwas', id=nr, body=document)


def es_query_illnesses(es, illnesses):
    s = Search(using = es, index = "gw")
    q = Q('bool', filter=[Q('match', MAPPED_TRAIT=illness) for illness in illnesses])
    s = s.query(q2)[0: 10000]
    
    res_dsl = s.execute().to_dict()
    return res_dsl

# This function get a JSON object  and transforms it into a bed file containing SNPs coordinates and associated trait
## TODO: the json_dict['hits'] contains only few documents with respect to the expected ones. Check why. 

def snp_json2bed_export(json_dict, export_name):
    with open(export_name, 'w+') as f:
        f.write("chr\tstart\tend\ttrait\n")
        for hit in json_dict['hits']['hits']:
            h = hit['_source']
            if (h['CHR_ID'] == 'X' or h['CHR_ID'] == 'Y'): h['CHR_ID'] = 23
            f.write("%s\t%s\t%s\t%s\n" %(int(h['CHR_ID']), int(h['CHR_POS']), int(h['CHR_POS'])+1, h['MAPPED_TRAIT']))

def main():
    es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
    # es_build(es) # This function call generate the ES database from JSON (to be used once). 

    # These tuples contains both input disease list file and the output file for BED export
    ALL_AI = (AI_DISEASE_TRAITS_RESTRICTED, AI_DISEASE_SNP_RESTRICTED)
    RA = (AI_DISEASE_RA, AI_DISEASE_SNP_RA) 
    T1D = (AI_DISEASE_T1D, AI_DISEASE_SNP_T1D)

    illnesses_list = file2list(T1D[0])
    print illnesses_list

    res_dsl = es_query_illnesses(es, illnesses_list)
    print res_dsl
    # Want to export data in BED format usig the snp_json2bed_export function.
    #snp_json2bed_export(res_dsl, T1D[1])

if __name__ == '__main__':
    main()
