# GWAS data
GWAS_CAA = '../data/gwas-catalog-associations.tsv' #gwas catalog associations
GWAS_CAA_ON = '../data/gwas-catalog-associations_ontology-annotated.tsv' # gwas catalog associations ontology-annotated
GWAS_CAS = '../data/gwas-catalog-studies.tsv' # gwas catalog studies
GWAS_CAS_ON = '../data/gwas-catalog-studies_ontology-annotated.tsv' # gwas catalog studies ontology annotated
GWAS_EFO_MAP = '../data/gwas-efo-trait-mappings.tsv' # gwas efo trait mappings.tsv
JSON_DOMAINS = '../data/json_domains.json'

# Disease traits data
ALL_DISEASES = '../data/all_diseases.txt'
AI_DISEASE_TRAITS = '../data/AI_disease_traits.txt'
AI_DISEASE_TRAITS_RESTRICTED = '../data/AI_disease_traits_restricted.txt'
AI_DISEASE_RA = '../data/AI_disease_RA.txt'
AI_DISEASE_T1D = '../data/AI_disease_T1D.txt'

# Exported BED files with SNPs coordinates and associated diesease
#ALL_DISEASES_SNP_BED = '../results/all_diseases_snp_bed.txt'
#AI_DISEASE_SNP_TRAITS = '../results/AI_disease_snp_traits.txt'
#AI_DISEASE_SNP_RESTRICTED = '../results/AI_disease_snp_restricted.txt'
#AI_DISEASE_SNP_RA = '../results/AI_disease_snp_RA.txt'
#AI_DISEASE_SNP_T1D = '../results/AI_disease_snp_T1D.txt'


# File mapping GWAS Cathalog SNP coordinates from hg38 to hg19
SNP_mapping_file = '../data/SNP_hg38_hg19.pkl'

# Dictionary with EFO mapping information for each Trait

EFO_GWAS_DICT = '../data/ego_gwas_dict.pkl'


# HG19
RESULTS_HG19 = '../results/hg_19/'
SNPs_HG19_RA = '../results/hg_19/SNPs/'

# Domains data
CCD = '../data/bed_hg19/GM12878_CCD.bed'
RAO = '../data/bed_hg19/GM12878_rao.bed'
IMR90 = '../data/bed_hg19/IMR90_hg19.bed'

# HG19 gene coordinates in tsv format.

REFSEQ_GENE_COORDS = '../data/gene_coords/RefSeq_hg19.tsv'
GENCODE_GENE_COORDS_B = '../data/gene_coords/GENCODE_hg19_basic.tsv'
GENCODE_GENE_COORDS_C = '../data/gene_coords/GENCODE_hg19_complete.tsv'


#Mongo DBs and collections
DB_NAME = 'GWAS_hg19'
COLLECTION_NAME = 'GWAS'
COLLECTION_NAME_DOMAINS = 'GWAS_DOMAINS'
COLLECTION_GWAS_GENES = 'GWAS_GENES'
COLLECTION_GWAS_TRAITS = 'GWAS_TRAITS'
COLLECTION_GWAS_CATEGORIES = 'GWAS_CATEGORIES'
COLLECTION_GWAS_CHR = 'GWAS_CHR'
GWAS_DB_JSON = '../results/GWAS_db.json'
