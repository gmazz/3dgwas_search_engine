from pymongo import MongoClient 
from path_config import DB_NAME, COLLECTION_NAME_DOMAINS 

class NestedDict(dict):
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value


def query(domain_from, domain_to, genes, traits, DB_name, collection_name):
    client = MongoClient()
    db = client[DB_name]
    collection = db[collection_name]
    query_dict = {}
    if len(genes) > 0:
        query_dict["genes"] = {"$all": genes} 
    if len(traits) > 0:
        query_dict["traits"] = {"$all": traits} 
    if domain_from != -1:
        query_dict["end"] = {"$gte": domain_from} 
    if domain_to != -1:
        query_dict["start"] = {"$lte": domain_to} 
    collection = collection.find(query_dict)
    return list(collection)


def response(res):
    reply = {
        "domains": [],
        "genes": [],
        "illnesses": []
    }
    for r in res:
        reply["domains"] = r["_id"]
        reply["genes"] += r["genes"]
        reply["illnesses"] += r["traits"]
    reply["genes"] = list(set(reply["genes"]))
    reply["illnesses"] = list(set(reply["illnesses"]))
    return reply


def test():
    genes = ['NHS']
    traits = ['Nephroblastoma']
    res = query(-1, 35739020, genes, traits, DB_NAME, COLLECTION_NAME_DOMAINS)
    print response(res)

if __name__ == '__main__':
    test()

