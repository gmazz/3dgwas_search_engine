from utils import *

'''
This program generates three new mongoDB collections in the GWAS_hg19 database.
Namely:
    1) GWAS_traits: collection of all the traits and related categories
    2) GWAS_categories: collection of all categories and related traits
    3) GWAS_chr: collection of all chromosomes
'''


def get_trait_list(): 
    efo_gwas_dict = pickle_load(EFO_GWAS_DICT)
    return [{'id':i, 
             'name':t, 
             'cat_names':list(set([j['Parent term'] for j in efo_gwas_dict[t]])), 
             'EFO_names':list(set([j['EFO term'] for j in efo_gwas_dict[t]])),
             } for i, t in enumerate(efo_gwas_dict)]


def get_cat_list():
    efo_gwas_dict = pickle_load(EFO_GWAS_DICT)
    categories = {}
    for t, entries in efo_gwas_dict.iteritems():
        for entry in entries:
            try:
                if t not in categories[entry['Parent term']]:
                    categories[entry['Parent term']].append(t)
                else:
                    pass
                    #print "{} is already in {}".format(t, categories[entry['Parent term']])
            except:
                categories[entry['Parent term']] = [t]

    return [{'id':i,
             'name':t,
             'trait_names': categories[t] 
             } for i, t in enumerate(categories)]


def main():
    
    
    trait_list = get_trait_list() 
    cat_list = get_cat_list()
    chr_list = [{'id': 0, 'chr_name': 'X'},
                {'id': 1, 'chr_name': '1'},
                {'id': 2, 'chr_name': '2'},
                {'id': 3, 'chr_name': '3'},
                {'id': 4, 'chr_name': '4'},
                {'id': 5, 'chr_name': '5'},
                {'id': 6, 'chr_name': '6'},
                {'id': 7, 'chr_name': '7'},
                {'id': 8, 'chr_name': '8'},
                {'id': 9, 'chr_name': '9'},
                {'id': 10, 'chr_name': '10'},
                {'id': 11, 'chr_name': '11'},
                {'id': 12, 'chr_name': '12'},
                {'id': 13, 'chr_name': '13'},
                {'id': 14, 'chr_name': '14'},
                {'id': 15, 'chr_name': '15'},
                {'id': 16, 'chr_name': '16'},
                {'id': 17, 'chr_name': '17'},
                {'id': 18, 'chr_name': '18'},
                {'id': 19, 'chr_name': '19'},
                {'id': 20, 'chr_name': '20'},
                {'id': 21, 'chr_name': '21'},
                {'id': 22, 'chr_name': '22'},
                ]

    # Parameters of the gene coordinate file of interest
    params = [{'db_name': DB_NAME, 'collection_name': COLLECTION_GWAS_TRAITS, 'data_list': trait_list},
            {'db_name': DB_NAME, 'collection_name': COLLECTION_GWAS_CATEGORIES, 'data_list': cat_list },
            {'db_name': DB_NAME, 'collection_name': COLLECTION_GWAS_CHR, 'data_list': chr_list }]

    for p in params:
        mongo_drop_collection(p['db_name'], p['collection_name'])
        mongo_insert( p['db_name'], p['collection_name'], p['data_list'])
    

if __name__ == '__main__':
    main()
