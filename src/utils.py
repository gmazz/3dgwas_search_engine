import os, re, sys, subprocess, csv, requests
from pymongo import MongoClient
import cPickle
import itertools
from path_config import *
from compiler.ast import flatten
import json
#from elasticsearch import Elasticsearch
#from elasticsearch_dsl import Search, Q


def file2list(file, **kwargs):
    sep = kwargs.get('sep', None)
    if sep:
        return [r.rsplit('\n')[0].split(sep) for r in open(file, 'r+').readlines()]
    else:
        return [r.rsplit('\n')[0] for r in open(file, 'r+').readlines()]

def csv2dict(csv_file, sep):
    input_file = csv.DictReader(open(csv_file), delimiter=sep)
    return [row for row in input_file]


def csv2dict_simple(csv_file, sep):
    simple_dict = {}
    input_file = csv.DictReader(open(csv_file), delimiter=sep)
    for indx, row in enumerate(input_file):
        simple_dict[indx] = row
    return simple_dict
    

def csv2dict_plus_list(csv_file, sep, additionals): # Accept a dict with additional fields .e.g. {"additional_one": 0, "additional_two" : True}
    enriched_list = []
    input_file = csv.DictReader(open(csv_file), delimiter=sep)
    for indx, row in enumerate(input_file):
        row['index'] = indx
        for k, v in additionals.iteritems():
            row[k] = v
        enriched_list.append(row)
    return enriched_list


def csv2dict_plus_dict(csv_file, sep): # Accept a dict with additional fields .e.g. {"additional_one": 0, "additional_two" : True}
    enriched_dict = {}
    input_file = csv.DictReader(open(csv_file), delimiter=sep)
    for indx, row in enumerate(input_file):
        row['index'] = indx
        row['AI_hits'] = 0
        row['AI_diseases'] = set()
        row['AI_genes'] = set()
        row['AI_pos'] = set()
        enriched_dict[indx] = row
    return enriched_dict


# Saves object by pickle on given path.
def pickle_save(obj, path):
    with open(path, 'wb') as save_file:
        cPickle.dump(obj, save_file)


# Loads object by pickle on given path.
def pickle_load(path):
    with open(path, 'rb') as load_file:
        return cPickle.load(load_file)


# This function insert a given list of dicts into a specific mongoDB collection
def mongoDB_insert(db_name, collection_name, data):
    try:
        client = MongoClient()
        db = client[db_name]
        collection = db[collection_name]
    except:
        print "Unable to connect to MongoDB"
    try:
        collection.insert(data)
        print "Your request were inserted in %s.%s" %(db_name, collection_name)
    except:
        print "Unfortunately your request couldn't be inserted in %s.%s" %(db_name, collection_name)


def mongo_insert(DB_name, collection_name, entry_list):
    mongoDB_insert (DB_name, collection_name, entry_list)


def mongo_drop_collection(db_name, collection_name):
    client = MongoClient()
    db = client[db_name]
    db.drop_collection(collection_name)


def json_dump(filename, data):
    with open(filename, "w") as filename:
        json.dump(data, filename, indent=4, separators=(',', ': '))
