from utils import *

'''
This script query the GWAS collection in the GWAS_hg19 database and
generates a new collection named GWAS_domains containing the aggregation of the GWAS SNPs by domains
'''

class NestedDict(dict):
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value


def get_domains_only(DB_name, collection_name):
    client = MongoClient()
    db = client[DB_name]
    collection = db[collection_name]
    return list(collection.find({"DOMAINS": {"$exists": "true", "$not": {"$size": 0}}}))


def list_flat(target):
    return list(set([x for y in [eval(i) for i in target] for x in y]))

def aggregate_by_domains(GWAS_only_domains):
    domain_collection = NestedDict()
    for i in GWAS_only_domains:
        d = i['DOMAINS'][0]  # In our case all SNPs have a single domain (no overlapping) 
        domain_collection[d['chr']][d['start']][d['end']]['traits'][i['MAPPED_TRAIT']] = 0
        domain_collection[d['chr']][d['start']][d['end']]['genes'][str(i['MAPPED_GENE'])] = 0 # Transformed i['MAPPED_GENE'], list -> str.
    GWAS_domain_list = []
    for chr in domain_collection:
        for start in domain_collection[chr]:
            for end in domain_collection[chr][start]:
                GWAS_domain_list.append({
                    'chr': chr,
                    'start': int(start),
                    'end': int(end),
                    'traits': domain_collection[chr][start][end]['traits'].keys(),
                    'genes': list_flat(domain_collection[chr][start][end]['genes'].keys())
                })
    return GWAS_domain_list


def include_domain_id(GWAS_domain_list):
    for domain_id, domain in enumerate(GWAS_domain_list):
        domain['domain_id'] = domain_id
    return GWAS_domain_list
    

def main():
    GWAS_only_domains = get_domains_only(DB_NAME, COLLECTION_NAME)
    GWAS_domain_list = aggregate_by_domains(GWAS_only_domains)
    GWAS_domain_list = include_domain_id(GWAS_domain_list)
    
    mongo_drop_collection(DB_NAME, COLLECTION_NAME_DOMAINS)
    mongo_insert(DB_NAME, COLLECTION_NAME_DOMAINS, GWAS_domain_list)
  

if __name__ == '__main__':
    main()
