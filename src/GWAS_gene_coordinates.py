from utils import *

'''
This program generates a new mongoDB collection in the GWAS_hg19 database.
The new collection contains the coordinates of genes present in the GWAS_hg19.GWAS collection 
'''

# Select only genes entries having domains
def get_domains_only(DB_name, collection_name):
    client = MongoClient()
    db = client[DB_name]
    collection = db[collection_name]
    return list(collection.find({"DOMAINS": {"$exists": "true", "$not": {"$size": 0}}}))


# Returns a unique list of genes from the GWAS_H19.GWAS collection
def get_GWAS_genes(GWAS_only_domains):
    return list(set([x for y in [i['MAPPED_GENE'] for i in GWAS_only_domains] for x in y]))


def build_GWAS_gene_list_slow(tmp_gene_list, hg19_gene_coords_f):
    l = csv2dict(hg19_gene_coords_f, '\t')
    GWAS_gene_list = [i for i in l if i['geneName'] in tmp_gene_list]
    print len(l), len(l2), len(tmp_gene_list)
    return GWAS_gene_list

'''
NOTE: Each given gene's "name_field" can have multiple associated entries (e.g. same "gene name" but slightly different properties').
Therfore for each given "name_field" all the associated entries are considered.  
'''
def build_GWAS_gene_list(tmp_gene_list, params):
    data_list = csv2dict(params['file_path'], '\t')
    d = {}
    for elem in data_list:
        try:
            d[elem[params['name_field']]].append(elem)
        except:
            d[elem[params['name_field']]] = [elem]

    GWAS_gene_list = []
    for name in tmp_gene_list:
        try:
            GWAS_gene_list.append({'gene_name': name, 'gene_data': d[name]})
        except:
            pass
            #print 'Gene {} not found in {}'.format(name, params['file_path'])
    return GWAS_gene_list


def main():
    GWAS_only_domains = get_domains_only(DB_NAME, COLLECTION_NAME)
    tmp_gene_list = get_GWAS_genes(GWAS_only_domains) 
    
    params_1 = {'name_field':'name2', 'file_path': GENCODE_GENE_COORDS_C}
    params_2 = {'name_field':'geneName', 'file_path': REFSEQ_GENE_COORDS} # Parameters of the gene coordinate file of interest

    GWAS_gene_list = build_GWAS_gene_list(tmp_gene_list, params_2)  
    mongo_drop_collection(DB_NAME, COLLECTION_GWAS_GENES)
    mongo_insert(DB_NAME, COLLECTION_GWAS_GENES, GWAS_gene_list)
  

if __name__ == '__main__':
    main()
